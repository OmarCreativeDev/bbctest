var myApp = angular.module('myApp',[]);

myApp.controller('mainCtrl', ['$scope', function($scope) {

	$scope.hideBuildings = true;
	$scope.hideDepartments = true;
	$scope.editMode = true;

	$scope.data = [
		{
			country: 'united kingdom',
			building: ['red building - UK', 'blue building - UK', 'white building - UK', 'purple building - UK'],
			division: 'military',
			department: ['air force', 'marines', 'commando unit', 'GI Joe unit']
		},
		{
			country: 'america',
			building: ['tall building - USA', 'short building - USA', 'funky building - USA', 'bizarre building - USA'],
			division: 'secret service',
			department: ['FBI, police', 'NSA', 'government spy department']
		},
		{
			country: 'australia',
			building: ['west building - AUS', 'EAST building - AUS', 'NORTH building - AUS', 'South building - AUS'],
			division: 'sports',
			department: ['football', 'rugby', 'diving', 'swimming']
		},
		{
			country: 'china',
			building: ['invisible building - CHN', 'visible building - CHN', 'tonys building - CHN', 'unknown building - CHN'],
			division: 'health services',
			department: ['nhs', 'surgical centre', 'physiotherapy', 'gp clinic']
		}
	];

	$scope.populateBuildings = function(object){
		if (typeof object !== 'undefined') {
			$scope.hideBuildings = false;
			$scope.country = object.country;
			$scope.buildingsArray = object.building;
		} else {
			$scope.hideBuildings = true;			
		}
	};

	$scope.getBuilding = function(object){
		if (typeof object !== 'undefined') {
			$scope.building = object;
		}
	};

	$scope.populateDepartments = function(object){
		if (typeof object !== 'undefined') {		
			$scope.hideDepartments = false;
			$scope.division = object.division;
			$scope.departmentsArray = object.department;
		} else {
			$scope.hideDepartments = true;
		}
	};

	$scope.getDepartment = function(object){
		if (typeof object !== 'undefined') {
			$scope.department = object;
		}
	};	

	$scope.detailsFormSubmit = function(){
		$scope.editMode = false;
	};

}]);